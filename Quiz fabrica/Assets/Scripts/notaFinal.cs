﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class notaFinal : MonoBehaviour
{

    private int idTema;
    public Text TxtNota;
    public Text TxtInfoTema;

    public GameObject Estrela1;
    public GameObject Estrela2;
    public GameObject Estrela3;
    private int notaF;
    private int acertos;

    // Start is called before the first frame update
    void Start()
    {
        idTema = PlayerPrefs.GetInt("idTema");

        Estrela1.SetActive(false);
        Estrela2.SetActive(false);
        Estrela3.SetActive(false);

        
        notaF = PlayerPrefs.GetInt("notaFinalTemp"+idTema.ToString());
        acertos = PlayerPrefs.GetInt("acertosTemp"+idTema.ToString());

        TxtNota.text = notaF.ToString();
        TxtInfoTema.text = "Você acertou " + acertos.ToString() + "/10 questões";

        if(notaF == 10)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(true);
            Estrela3.SetActive(true);
        }
        else if(notaF >= 7)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(true);
            Estrela3.SetActive(false);
        }
        else if(notaF >= 3)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(false);
            Estrela3.SetActive(false);
        }
    }

    public void JogarNovamente()
    {
        Application.LoadLevel("T" + idTema.ToString());
    }

}
