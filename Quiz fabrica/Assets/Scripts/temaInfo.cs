﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class temaInfo : MonoBehaviour
{

    public int idTema;

    public GameObject Estrela1;
    public GameObject Estrela2;
    public GameObject Estrela3;

    private int notaFinal;



    // Start is called before the first frame update
    void Start()
    {
        Estrela1.SetActive(false);
        Estrela2.SetActive(false);
        Estrela3.SetActive(false);

        int notaFinal = PlayerPrefs.GetInt("notaFinal" + idTema.ToString());

        if (notaFinal == 10)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(true);
            Estrela3.SetActive(true);
        }
        else if (notaFinal >= 7)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(true);
            Estrela3.SetActive(false);
        }
        else if (notaFinal >= 3)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(false);
            Estrela3.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
