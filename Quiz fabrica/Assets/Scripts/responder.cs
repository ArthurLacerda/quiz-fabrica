﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class responder : MonoBehaviour
{

    private int idTema;

    public Text Pergunta;
    public Text Resposta1;
    public Text Resposta2;
    public Text Resposta3;
    public Text Resposta4;
    public Text InfoPergunta;

    public string[] perguntas;          //Armazena todas as perguntas
    public string[] Alternativa1;       //Armazena todas as respostas número 1
    public string[] Alternativa2;       //Armazena todas as respostas número 2
    public string[] Alternativa3;       //Armazena todas as respostas número 3
    public string[] Alternativa4;       //Armazena todas as respostas número 4
    public string[] AlternativaCorreta; //Armazena todas as respostas corretas

    private int idPergunta;

    private int acertos;
    private int questoes;
    private int notaFinal;

    // Start is called before the first frame update
    void Start()
    {
        idTema = PlayerPrefs.GetInt("idTema");
        idPergunta = 0;
        questoes = perguntas.Length;

        Pergunta.text = perguntas[idPergunta];
        Resposta1.text = Alternativa1[idPergunta];
        Resposta2.text = Alternativa2[idPergunta];
        Resposta3.text = Alternativa3[idPergunta];
        Resposta4.text = Alternativa4[idPergunta];
        InfoPergunta.text = "Respondendo pergunta " + (idPergunta + 1) + "/" + questoes.ToString();

    }

    public void resposta(string alternativa)
    {
        if(alternativa == "1")
        {
            if(Alternativa1[idPergunta] == AlternativaCorreta[idPergunta])
            {
                acertos += 1;
            }
        }
        else if (alternativa == "2")
        {
            if (Alternativa2[idPergunta] == AlternativaCorreta[idPergunta])
            {
                acertos += 1;
            }
        }
        else if (alternativa == "3")
        {
            if (Alternativa3[idPergunta] == AlternativaCorreta[idPergunta])
            {
                acertos += 1;
            }
        }
        else if (alternativa == "4")
        {
            if (Alternativa4[idPergunta] == AlternativaCorreta[idPergunta])
            {
                acertos += 1;
            }
        }
        proximaPergunta();
    }

    void proximaPergunta()
    {
        idPergunta += 1;

        if(idPergunta <= (questoes - 1))
        {
            Pergunta.text = perguntas[idPergunta];
            Resposta1.text = Alternativa1[idPergunta];
            Resposta2.text = Alternativa2[idPergunta];
            Resposta3.text = Alternativa3[idPergunta];
            Resposta4.text = Alternativa4[idPergunta];

            InfoPergunta.text = "Respondendo pergunta " + (idPergunta + 1) + "/" + questoes.ToString();
        }
        else
        {
            //TERMINO DE PERGUNTAS
            notaFinal = acertos;

            if (notaFinal > PlayerPrefs.GetInt("notaFinal" + idTema.ToString()))
            {
                PlayerPrefs.SetInt("notaFinal"+idTema.ToString(), notaFinal);
                PlayerPrefs.SetInt("acertos"+idTema.ToString(), acertos);
            }

            PlayerPrefs.SetInt("notaFinalTemp" + idTema.ToString(), notaFinal);
            PlayerPrefs.SetInt("acertosTemp" + idTema.ToString(), acertos);



            Application.LoadLevel("NotaFinal");

        }
    }


}
