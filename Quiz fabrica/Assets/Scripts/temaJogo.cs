﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class temaJogo : MonoBehaviour
{
    public Button       BtnPlay;
    public Text         TxtNomeTema;

    public GameObject   InfoTema;
    public Text         TxtInfoTema;
    public GameObject   Estrela1;
    public GameObject   Estrela2;
    public GameObject   Estrela3;

    public string[]     nomeTema;
    public int          numeroQuestoes;

    private int         idTema;


    // Start is called before the first frame update
    void Start()
    {
        idTema = 0;
        TxtNomeTema.text = nomeTema[idTema];
        TxtInfoTema.text = "";

        InfoTema.SetActive(false);
        Estrela1.SetActive(false);
        Estrela2.SetActive(false);
        Estrela3.SetActive(false);
        BtnPlay.interactable = false;
    }


    public void selecioneTema(int i)
    {
        idTema = i;
        PlayerPrefs.SetInt("idTema", idTema);
        TxtNomeTema.text = nomeTema[idTema];

        int notaFinal = PlayerPrefs.GetInt("notaFinal" + idTema.ToString());
        int acertos = PlayerPrefs.GetInt("acertos" + idTema.ToString());

        Estrela1.SetActive(false);
        Estrela2.SetActive(false);
        Estrela3.SetActive(false);

        if (notaFinal == 10)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(true);
            Estrela3.SetActive(true);
        }
        else if (notaFinal >= 7)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(true);
            Estrela3.SetActive(false);
        }
        else if (notaFinal >= 3)
        {
            Estrela1.SetActive(true);
            Estrela2.SetActive(false);
            Estrela3.SetActive(false);
        }

        TxtInfoTema.text = "Você acertou " + acertos.ToString()+ "/" + numeroQuestoes.ToString() + " Questões!";
        InfoTema.SetActive(true);
        BtnPlay.interactable = true;
    }

    public void jogar()
    {
        Application.LoadLevel("T" + idTema.ToString());
    }

}
